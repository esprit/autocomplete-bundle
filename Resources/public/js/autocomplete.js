$(function () {
    $.fn.select2Autocomplete = function (options, initValues) {

        this.each(function () {
            let self = $(this);
            if (typeof self.data('select2-autocomplete') !== 'undefined') {
                return;
            };

            var form   = self.closest('form'),
                url    = form.attr('action') || window.location.href,
                values = initValues || self.data('value');

            var defaults = {
                //theme: "bootstrap",
                allowClear: true,
                language  : "ru",
                ajax      : {
                    url           : url,
                    dataType      : 'json',
                    delay         : 250,
                    method        : form.attr('method'),
                    cache         : false,
                    data          : function (params) {
                        var data = form.serialize();
                        return data +
                            '&q=' + (encodeURIComponent(params.term || '')) +
                            '&page=' + (params.page || 0) +
                            '&autocomplete-field=' + encodeURIComponent(self.data('name'));
                    },
                    processResults: function (data, page) {
                        return {results: data.items};
                    }
                },
                escapeMarkup: function(m) {
                    // Do not escape HTML in the select options text
                    return m;
                }
            };

            // extends from globals options
            if ($.fn.select2AutocompleteDefaults) {
                defaults = $.extend(true, defaults, $.fn.select2AutocompleteDefaults);
            }

            // support data attributes for select2@v4
            options = $.extend({
                tags: !!self.data('tags') || false,
                minimumInputLength: self.data('minimum-input-length') || 0,
                placeholder: self.data('placeholder') || '',
            }, options);

            if (values) {
                $.each(values, function (k, v) {
                    self.append(
                        $('<option>').val(v.id).html(v.text).prop('selected', true)
                    );
                });
            }

            self.data('select2-autocomplete', self.select2($.extend(defaults, options)));
        });

        return this;
    };

    $('.select2-autocomplete').select2Autocomplete();
});
