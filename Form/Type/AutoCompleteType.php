<?php

namespace AutocompleteBundle\Form\Type;

use AutocompleteBundle\Form\DataTransformer\EntityToAutocompleteDataTransformer;
use AutocompleteBundle\Service\PropertyResolver;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class AutoCompleteType extends AbstractType
{
    private $em;

    /** @var PropertyAccessor */
    private $propertyAccessor;

    /** @var PropertyResolver */
    private $propertyResolver;

    public function __construct(EntityManager $em, PropertyResolver $propertyResolver)
    {
        $this->em = $em;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
        $this->propertyResolver = $propertyResolver;
    }

    public function getParent()
    {
        return CustomChoiceType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => [],
            'class' => null,
            'identifier_property' => 'id',
            'query_builder' => null,
            'group_by' => null,
            'text_property' => null,
            'search_fields' => [],
            'items' => null,
            'autocomplete_key' => null,
            'allow_add' => null,
            'minimum_input_length' => 1,
        ]);

        //$resolver->setRequired('query_builder');
        $resolver->setRequired('class');
        $resolver->setRequired('autocomplete_key');
        $resolver->setAllowedTypes('text_property', ['null', 'string', 'callable']);
        $resolver->setAllowedTypes('autocomplete_key', ['string']);
        $resolver->setAllowedTypes('allow_add', ['null', 'callable']);
        $resolver->setAllowedTypes('minimum_input_length', ['int']);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->resetViewTransformers();

        if ($options['items']) {
            return;
        }

        $transformer = new EntityToAutocompleteDataTransformer(
            $this->em,
            $this->propertyResolver,
            $options);

        $builder->addViewTransformer($transformer);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['attr']['class'] = 'select2-autocomplete';
        $view->vars['attr']['data-name'] = $options['autocomplete_key'];

        if (! empty($options['attr']['class'])) {
            $view->vars['attr']['class'] .= ' ' . $options['attr']['class'];
        }

        if ($options['allow_add']) {
            $view->vars['attr']['data-tags'] = true;
        }

        if (($length = $options['minimum_input_length']) > 0) {
            $view->vars['attr']['data-minimum-input-length'] = $length;
        }
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $data = $form->getData();

        if ($data) {
            $value = $this->getValue($data, $options);
            $view->vars['attr']['data-value'] = sprintf("%s", json_encode($value, JSON_UNESCAPED_UNICODE));
        }
    }

    public function getValue($object, $options)
    {
        $values = [];

        if ($options['multiple']) {
            foreach ($object as $entity) {
                $values[] = $this->propertyResolver->getValue($entity, $options);
            }
        } else {
            $values[] = $this->propertyResolver->getValue($object, $options);
        }

        return $values;
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'autocomplete';
    }
}
