<?php

namespace AutocompleteBundle\Form\DataTransformer;

use AutocompleteBundle\Service\PropertyResolver;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\PropertyAccess\PropertyPath;

class EntityToAutocompleteDataTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var array
     */
    private $options;

    /**
     * @var PropertyPath the property path to use as value
     */
    private $identifier;

    private $propertyResolver;

    /**
     * @param EntityManager $em
     * @param string $class
     * @param PropertyPath $identifier
     */
    public function __construct(EntityManager $em, PropertyResolver $propertyResolver, $options)
    {
        $this->manager = $em;
        $this->propertyResolver = $propertyResolver;
        $this->identifier = new PropertyPath($options['identifier_property']);
        $this->options = $options;
    }

    /**
     * @param object|null $value The selected entity object
     *
     * @return mixed The value by which we are selecting
     */
    public function transform($value)
    {
        if ($this->options['multiple']) {

            if (null === $value) {
                return [];
            }

            $result = [];

            foreach ($value as $entity) {
                $result[] = $this->propertyResolver->getValue($entity, $this->options);
            }

            return $result;
        }

        if (null === $value) {
            return null;
        }

        return $this->propertyResolver->getValue($value, $this->options);
    }

    /**
     * @param mixed $value The value by which we are selecting
     *
     * @return object|null The resulting object
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return $this->options['multiple']
                ? []
                : null;
        }

        $element = $this->identifier->getElement(0);

        $getEntity = function ($v) use ($element) {
            $entity = $this->manager->getRepository($this->options['class'])->findOneBy(array($element => $v));

            if (! $entity and $createNew = $this->options['allow_add']) {
                $entity = $createNew($v);
            }

            return $entity;
        };

        return $this->options['multiple']
            ? array_map($getEntity, $value)
            : $getEntity($value);
    }
}
