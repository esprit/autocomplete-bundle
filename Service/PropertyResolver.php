<?php


namespace AutocompleteBundle\Service;

use Symfony\Component\PropertyAccess\PropertyAccess;

class PropertyResolver
{
    private $propertyAccess;

    public function __construct()
    {
        $this->propertyAccess = PropertyAccess::createPropertyAccessor();
    }

    public function resolveTextProperty($object, $textProperty): string
    {
        if (is_string($textProperty)) {
            $text = $this->propertyAccess->getValue($object, $textProperty);
        } elseif (is_callable($textProperty)) {
            $text = call_user_func($textProperty, $object);
        } else {
            $text = (string) $object;
        }

        return $text;
    }

    public function resolveIdProperty($object, string $idProperty): string
    {
        return $this->propertyAccess->getValue($object, $idProperty);
    }

    public function getValue($object, array $options)
    {
        $textProperty = $options['text_property'];
        $idProperty = $options['identifier_property'];
        return [
            'id'   => $this->resolveIdProperty($object, $idProperty),
            'text' => $this->resolveTextProperty($object, $textProperty),
        ];
    }
}