<?php

namespace AutocompleteBundle\Service;

use AutocompleteBundle\Form\Type\AutoCompleteType;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Usage example:
 * <code>
 *  $autocomplete = $this->get('autocomplete_handler');
 *  if ($autocomplete->isAutocomplete($request)) {
 *      return $autocomplete->getResponse($form);
 *  }
 * </code>
 */
class AutocompleteHandler extends AbstractTypeExtension
{
    private $em;

    /** @var LoggerInterface */
    private $logger;

    /** @var PropertyResolver */
    private $propertyResolver;

    private const REQUEST_PARAM = 'autocomplete-field';
    private $config = [];

    public function __construct(EntityManager $em, LoggerInterface $logger, PropertyResolver $propertyResolver)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->propertyResolver = $propertyResolver;
    }

    public function handleRequest(Request $request): ?JsonResponse
    {
        if ($this->isAutocomplete($request)) {
            return $this->getResponse($request);
        }

        return null;
    }

    private function isAutocomplete(Request $request): bool
    {
        return (bool) $request->get(self::REQUEST_PARAM);
    }

    private function getResponse(Request $request): JsonResponse
    {
        $key = $request->get(self::REQUEST_PARAM);
        $options = $this->config[$key];

        $query = $request->get('q');

        $itemsCallback = $options['items'];
        if ($itemsCallback) {
            // TODO add groups support
            $data = call_user_func($itemsCallback, $query);
            return new JsonResponse(['items' => $data]);
        }

        $queryBuilder = $options['query_builder'];
        $class = $options['class'];
        $groupBy = $options['group_by'];
        $searchFields = $options['search_fields'];

        if (! $queryBuilder and ! $searchFields) {
            throw new \Exception('Query builder or search fields should be defined');
        }

        if ($queryBuilder) {
            $result = $queryBuilder($this->em->getRepository($class), $query)->getQuery()->getResult();
        } elseif ($searchFields) {
            // TODO refactoring
            $qb = $this->em->getRepository($class)->createQueryBuilder('r');
            foreach ($searchFields as $field) {
                $qb->orWhere(sprintf("r.%s like :query", $field));
            }
            $qb->setParameter('query', '%' . $query . '%');
            $query = $qb->getQuery();
            $this->logger->debug('Create query for autocomplete form fields: ' . $query->getDQL());
            $result = $query->getResult();
        }

        $data = [];
        if ($groupBy) {

            $groups = [];
            foreach ($result as $r) {
                $parentText = $this->propertyResolver->resolveTextProperty($r, $groupBy);
                $groups[$parentText][] = $this->propertyResolver->getValue($r, $options);
            }

            foreach ($groups as $parent => $children) {
                $data[] = [
                    'text'     => $parent,
                    'children' => $children,
                ];
            }
        } else {
            foreach ($result as $r) {
                $data[] = $this->propertyResolver->getValue($r, $options);
            }
        }

        return new JsonResponse(['items' => $data]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $key = $options['autocomplete_key'];
        $this->logger->debug('Add autocomplete configuration for "{key}"', ['key' => $key]);
        $this->config[$key] = $options;
    }

    public static function getExtendedTypes(): iterable
    {
        yield AutoCompleteType::class;
    }
}
